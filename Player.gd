extends KinematicBody

export var speed = 10
export var acceleration = 5
export var gravity = 0.98
export var jump_power = 30
export var mouse_sensitivity = 0.3

onready var head = $Head
onready var camera = $Head/Camera

var velocity = Vector3()
var camera_x_rotation = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#print("Running _process()")
	_physics_process(delta)

# Process 1 cycle of physics
func _physics_process(delta):
	process_keyboard_input(delta)

# Handle mouse input
func _input(event):
	if event is InputEventMouseMotion:
		head.rotate_y(deg2rad(-event.relative.x * mouse_sensitivity))
		
		# Check if x-axis rotation is straight up or straight down,
		# prevents player camera from rolling over the x-axis
		var x_delta = event.relative.y * mouse_sensitivity
		if camera_x_rotation + x_delta > -90 and camera_x_rotation + x_delta < 90:
			camera.rotate_x(deg2rad(-x_delta))
			camera_x_rotation += x_delta

# Handle keyboard input
func process_keyboard_input(delta):
	var head_xform = head.get_global_transform().basis
	var input_movement = Vector3()

	# Check WASD
	if Input.is_action_pressed("move_forward"):
		input_movement -= head_xform.z
		print("Moving forward")
	elif Input.is_action_pressed("move_backward"):
		input_movement += head_xform.z

	if Input.is_action_pressed("move_left"):
		input_movement -= head_xform.x
	elif Input.is_action_pressed("move_right"):
		input_movement += head_xform.x

	if Input.is_action_just_pressed("jump"):
		input_movement += head_xform.y

	input_movement = input_movement.normalized()
	
	velocity = velocity.linear_interpolate(input_movement * speed, acceleration * delta)
	velocity = move_and_slide(velocity)

	print("input_movement: %s, velocity: %s" % [str(input_movement), str(velocity)])
